package br.com.itau;

import java.util.Scanner;

public class Impressora {
    Scanner scanner = new Scanner(System.in);
    Fatorial fatorial = new Fatorial();

    public Fatorial exibeFatorial() {
        System.out.print("Digite o número que deseja calcular a fatorial (valor máximo = 20): ");
        fatorial.setValorInicial(scanner.nextInt());
        return this.fatorial;
    }

    public void imprimeFatorial(Fatorial fatorial) {
        System.out.println("O valor da fatorial é " + fatorial.getValorFinal() + "!");
    }

}
