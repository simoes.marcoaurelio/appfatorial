package br.com.itau;

public class Main {
    public static void main(String[] args) {

        Impressora impressora = new Impressora();
        Fatorial fatorial;

        fatorial = impressora.exibeFatorial();

        fatorial.validaEntrada();

        if(fatorial.getCalculoPossivel()) {
            fatorial.calculaFatorial();
            impressora.imprimeFatorial(fatorial);
        } else {
            System.out.println("Fim do programa");
        }
    }
}
