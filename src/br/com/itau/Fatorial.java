package br.com.itau;

public class Fatorial {

    private int valorInicial;
    private long valorFinal;
    private boolean calculoPossivel;

    public void setValorInicial(int valorInicial) {
        this.valorInicial = valorInicial;
    }

    public long getValorFinal() {
        return valorFinal;
    }

    public boolean getCalculoPossivel() {
        return calculoPossivel;
    }

    public void validaEntrada() {
        if (this.valorInicial > 20) {
            System.out.println("Valor acima do permitido!");
            this.calculoPossivel = false;
        } else {
            this.calculoPossivel = true;
        }
    }

    public void calculaFatorial() {

        long valorAtual = this.valorInicial;

        for (int contador = 1; contador < this.valorInicial; contador++) {
            valorAtual = valorAtual * contador;

            this.valorFinal = valorAtual;
        }
    }

}
